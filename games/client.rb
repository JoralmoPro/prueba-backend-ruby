# Reto 2
require 'net/http'
require 'json'

p "pid del juego"
pid = gets.chomp

datosDelJuego = Net::HTTP.get_response('localhost', '/hangman/' + pid, 9292)

datosDelJuego = JSON.parse(datosDelJuego.body)

while(datosDelJuego[":state"] == "playing")
  puts `clear`
  p "Ingresa una letra"
  letra = gets.chomp
  datosDelJuego = Net::HTTP.get_response('localhost', '/hangman/' + pid + '/try/' + letra, 9292)
  datosDelJuego = JSON.parse(datosDelJuego.body)
  puts JSON.pretty_generate(datosDelJuego)
  p "Enter para continuar"
  gets.chomp
end

if datosDelJuego[":state"] == "lose"
  p "========================"
  p "Perdiste"
  p "========================"
else
  p "========================"
  p "Ganaste"
  p "========================"
end
# Fin Reto 2