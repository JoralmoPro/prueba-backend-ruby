#Reto 3
module Games
  class PuzzleApi < Sinatra::Base
    get '/puzzle/:pid/:n' do
      @game = Puzzle.where(pid: params[:pid]).first
      if @game.nil?
        n = params[:n].to_i
        limite = n*n
        x = 0
        y = 0
        numerosOrdenados = (0..limite - 1).to_a
        numeros = (0..limite - 1).to_a.shuffle

        m = Array.new(n) { Array.new(n, 0) }
        mC = Array.new(n) { Array.new(n, 0) }

        m.each_with_index do |e, row|
          e.each_with_index do |f, col|
            m[row][col] = numeros.pop
            mC[row][col] = numerosOrdenados.pop
            if m[row][col] == 0 
              x = row
              y = col
            end
          end
        end

        @game = Puzzle.new(
          pid: params[:pid],
          matriz: m,
          matrizOrdenada: mC,
          ceroX: x,
          ceroY: y,
          tam: n
        )
        @game.save
      end

      res = "<br>"
      res2  = "<br>"

      @game.matriz.each_with_index do |e, row, col|
        e.each_with_index do |f, col,|
          res += @game.matriz[row][col].to_s + " "
          res2 += @game.matrizOrdenada[row][col].to_s + " "
        end
        res += "<br>"
        res2 += "<br>"
      end

      Oj.dump(
        matriz: res,
        objetivo: res2,
        posicion: {x: @game.ceroX, y: @game.ceroY},
        tam: @game.tam
      )

    end
    get '/puzzle/:pid/jugar/:movimiento' do
      errror = ""
      ganado = false
      @game = Puzzle.where(pid: params[:pid]).first
      if @game.nil?
        Oj.dump(
          error: "El juego no existe"
        )
      else
        if params[:movimiento] == "derecha"
          if(@game.ceroY != @game.tam - 1)
            temp = @game.matriz[@game.ceroX][@game.ceroY+1]
            @game.matriz[@game.ceroX][@game.ceroY+1] = 0
            @game.matriz[@game.ceroX][@game.ceroY] = temp
            @game.ceroY = @game.ceroY + 1
            @game.save
          else
            error = "Movimiento no permitido"
          end
        elsif params[:movimiento] == "izquierda"
          if(@game.ceroY != 0)
            temp = @game.matriz[@game.ceroX][@game.ceroY-1]
            @game.matriz[@game.ceroX][@game.ceroY-1] = 0
            @game.matriz[@game.ceroX][@game.ceroY] = temp
            @game.ceroY = @game.ceroY - 1
            @game.save
          else
            error = "Movimiento no permitido"
          end
        elsif params[:movimiento] == "abajo"
          if(@game.ceroX != @game.tam - 1)
            temp = @game.matriz[@game.ceroX+1][@game.ceroY]
            @game.matriz[@game.ceroX+1][@game.ceroY] = 0
            @game.matriz[@game.ceroX][@game.ceroY] = temp
            @game.ceroX = @game.ceroX + 1
            @game.save
          else
            error = "Movimiento no permitido"
          end
        elsif params[:movimiento] == "arriba"
          if(@game.ceroX != 0)
            temp = @game.matriz[@game.ceroX-1][@game.ceroY]
            @game.matriz[@game.ceroX-1][@game.ceroY] = 0
            @game.matriz[@game.ceroX][@game.ceroY] = temp
            @game.ceroX = @game.ceroX - 1
            @game.save
          else
            error = "Movimiento no permitido"
          end
        end
        
        res = "<br>"
        res2  = "<br>"

        @game.matriz.each_with_index do |e, row, col|
          e.each_with_index do |f, col,|
            res += @game.matriz[row][col].to_s + " "
            res2 += @game.matrizOrdenada[row][col].to_s + " "
          end
          res += "<br>"
          res2 += "<br>"
        end

        if res == res2
          ganado = true
        end

        Oj.dump(
          matriz: res,
          objetivo: res2,
          posicion: {x: @game.ceroX, y: @game.ceroY},
          tam: @game.tam,
          errror: error,
          ganado: ganado
        )

      end


    end
  end
end
#Fin Reto 3
