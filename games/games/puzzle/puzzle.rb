module Games
  class Puzzle
    include Mongoid::Document

    field :matriz, type: Array, default: []
    field :matrizOrdenada, type: Array, default: []
    field :ceroX, type: Integer
    field :ceroY, type: Integer
    field :tam, type: Integer
    field :pid, type: String
    index({ pid: 1 }, unique: true)
  end
end
