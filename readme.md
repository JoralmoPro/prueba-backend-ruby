## Reto 1
### Se ha modificado el archivo /hangman/api.rb
```rb
    # Reto 1
      if @game.f > 3
        r = Oj.load(r)
        r[:hint] = @game.h
        r = Oj.dump(r)
      end
    # Fin Reto 1
```


## Reto 2
### se ha creado el cliente en la carpeta raiz llamado client.rb
```sh
    ruby client.rb
```
    pide el pid del juego
    y luego va pidiendo letra por letra hasta que el juego se gane o se pierda
    
## Reto 3
### Se ha creado los archivos /puzzle/api.rb (para las rutas del juego) y /puzzle/puzzle.rb (para el modelo del juego)
#### con las siguientes rutas:
    /puzzle/:pid/:n 
#### que recibe el pid del juego y el numero de columnas y filas de la matriz
    /puzzle/:pid/jugar/:movimiento 
#### que recibe el pid del juego y el tipo de movimiento a realizar [derecha, izquierda, arriba, abajo] ej:
    /puzzle/juego1/jugar/derecha (Movera a la derecha)
    /puzzle/juego1/jugar/arriba (Movera a arriba)